;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Utilities

;; It's _that_ library

(library (d-bus protocol utils)
  (export
    string-split fxalign)
  (import
    (rnrs))

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

(define (fxalign i alignment)
  (fxand (fx+ i (fx- alignment 1))
         (fx- alignment))))
