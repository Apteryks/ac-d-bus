;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Connection setup and authentication

(library (d-bus protocol connections)
  (export
    d-bus-conn?
    d-bus-connect
    d-bus-session-bus-address
    d-bus-system-bus-address
    d-bus-conn-uuid                     ;_not_ an RFC4122 UUID
    d-bus-conn-socket
    d-bus-conn-inport
    d-bus-conn-fd-dequeue
    d-bus-conn-unix-fds-supported?
    d-bus-conn-next-serial!
    d-bus-conn-flush
    d-bus-disconnect)
  (import
    (rnrs)
    (rnrs mutable-strings)
    (srfi :98 os-environment-variables)
    (d-bus compat socket)
    (d-bus compat uid)
    (d-bus protocol signatures)
    (d-bus protocol utils)
    (d-bus protocol wire))

(define debug #f)

(define-record-type d-bus-conn
  (sealed #t)
  (fields uuid socket inport fd-dequeue
          (mutable serial))
  (protocol
   (lambda (p)
     (lambda (uuid socket unix-fds-supported?)
       (let-values ([(inport fd-dequeue) (socket-input-port/fds socket)])
         (let ((serial 0))
           (p uuid socket inport (and unix-fds-supported? fd-dequeue) serial)))))))

(define (d-bus-conn-unix-fds-supported? conn)
  (and (d-bus-conn-fd-dequeue conn) #t))

(define (d-bus-conn-next-serial! conn)
  (let* ((serial (bitwise-and (+ 1 (d-bus-conn-serial conn)) #xffffffff))
         (serial (if (eqv? serial 0) 1 serial)))
    (d-bus-conn-serial-set! conn serial)
    serial))

(define (address-unescape x)
  ;; FIXME: implement %-unescaping
  x)

(define (d-bus-session-bus-address)
  (get-environment-variable "DBUS_SESSION_BUS_ADDRESS"))

(define (d-bus-system-bus-address)
  "unix:path=/var/run/dbus/system_bus_socket")

;; Returns something like: ((abstract . "filename") (guid . "guid"))
(define (d-bus-parse-address addr)
  (if (and (> (string-length addr) (string-length "unix:"))
           (string=? "unix:" (substring addr 0 (string-length "unix:"))))
      `((type . unix)
        ,@(map (lambda (var)
                 (let* ((x (string-split var #\=))
                        (name (string->symbol (car x)))
                        (value (address-unescape (cadr x))))
                   (cons name value)))
               (string-split (substring addr (string-length "unix:")
                                        (string-length addr))
                             #\,)))
      (error 'd-bus-session-bus-address "Unimplemented address type" addr)))

(define (get-ascii port)
  (let ((line (get-line port)))
    (cond ((eof-object? line)
           line)
          (else
           (let ((resp (string-split line #\space)))
             (when debug
               (display "← ")
               (write resp)
               (newline))
             resp)))))

(define (send-ascii port command)
  (when debug
    (display "→ ")
    (write command)
    (newline))
  (put-string port command)
  (newline port)
  (flush-output-port port))

(define (base16 x)
  (define hex "0123456789abcdef")
  (do ((ret (make-string (* 2 (string-length x))))
       (i 0 (+ i 1)))
      ((= i (string-length x))
       ret)
    (let-values ([(h l) (fxdiv-and-mod (char->integer (string-ref x i)) 16)])
      (string-set! ret (* i 2) (string-ref hex h))
      (string-set! ret (+ (* i 2) 1) (string-ref hex l)))))

;; Get the user id (e.g. 1000) and translate it to base16.
(define (user-uid-base16)
  (base16 (number->string (user-effective-uid) 10)))

;; Connect and authenticate to the dbus server.
(define d-bus-connect
  (case-lambda
    (()
     (d-bus-connect (or (d-bus-session-bus-address)
                        (d-bus-system-bus-address))))
    ((addr)
     (let* ((address (d-bus-parse-address addr))
            (filename
             (cond ((assq 'abstract address) =>
                    (lambda (x)
                      (string-append "\x0;" (cdr x))))
                   ((assq 'path address) => cdr)
                   (else (error 'd-bus-connect "Unknown address type" addr))))
            (sock (local-connect filename)))
       (guard (exn (else (socket-close sock) (raise exn)))
         (let-values ([(i o) (socket-textual-ports sock)])
           (put-char o #\nul)            ;gopher countermeasure
           (send-ascii o (string-append "AUTH EXTERNAL " (user-uid-base16)))
           (let ((uuid
                  (let lp ((tried '("EXTERNAL")))
                    (let ((resp (get-ascii i)))
                      (cond
                        ((equal? (car resp) "OK")
                         (let ((uuid (cadr resp)))
                           uuid))
                        ((and (equal? (car resp) "REJECTED")
                              (member "ANONYMOUS" (cdr resp))
                              (not (member "ANONYMOUS" tried)))
                         (send-ascii o "AUTH ANONYMOUS")
                         (lp (cons "ANONYMOUS" tried)))
                        (else
                         (error 'd-bus-connect "Connection failed" resp)))))))
             (letrec ((negotiate-unix-fds
                       (lambda ()
                         (cond ((not (socket-can-pass-fds? sock))
                                #f)
                               (else
                                (send-ascii o "NEGOTIATE_UNIX_FD")
                                (equal? (get-ascii i) '("AGREE_UNIX_FD")))))))
               (let ((unix-fds-supported (negotiate-unix-fds)))
                 ;; After BEGIN has been sent the textual ports are no
                 ;; longer used. Everything from this point on is binary,
                 ;; possibly with encryption.
                 (send-ascii o "BEGIN")
                 (flush-output-port o)
                 (make-d-bus-conn uuid sock unix-fds-supported))))))))))

(define (d-bus-conn-flush conn)
  #f)                                   ;nothing for now, but part of the API

(define (d-bus-disconnect conn)
  (socket-close (d-bus-conn-socket conn))))
