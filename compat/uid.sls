;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Get the effective user id

(library (d-bus compat uid)
  (export
    user-effective-uid)
  (import
    (rnrs)
    (pffi))

(define lib
  (open-shared-object "libacdbuscompat.so"))

(define user-effective-uid
  (foreign-procedure lib int acdbus_geteuid ())))
