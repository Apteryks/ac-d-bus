/* -*- mode: c; coding: utf-8 -*-
 * SPDX-License-Identifier: MIT
 * Copyright © 2020 Göran Weinholt
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

/* Connect to a Unix domain socket.
 *
 * Returns a non-negative number for a connected file descriptor.
 * Returns a negative number for errors (negative errno).
 */
int acdbus_connect(char *path, size_t pathlen)
{
    int sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0) {
        return -errno;
    }

    struct sockaddr_un addr = {
        .sun_family = AF_UNIX,
    };

    if (pathlen > sizeof(addr.sun_path)) {
        return -E2BIG;
    }

    memcpy(addr.sun_path, path, pathlen);

    int ret = connect(sock, (const struct sockaddr *) &addr, sizeof(addr));
    if (ret < 0) {
        int err = errno;
        close(sock);
        return -err;
    }

    return sock;
}

/* Receive data and fds on a Unix domain socket
 *
 * fdbuf is an array of ints, fdbuf[0] will be the number of fds received
 */
int acdbus_recv(int fd, char *bv, size_t start, size_t count, int *fdbuf, int fdbuflen)
{
    struct iovec io = {
        .iov_base = bv + start,
        .iov_len = count,
    };
    union {
        char buf[CMSG_SPACE(sizeof(int)*(fdbuflen-1))];
        struct cmsghdr align;
    } u;
    struct msghdr msg = {
        .msg_iov = &io,
        .msg_iovlen = 1,
        .msg_control = u.buf,
        .msg_controllen = sizeof u.buf,
    };

    int ret = recvmsg(fd, &msg, 0);
    if (ret < 0) {
        return -errno;
    }

    memset(fdbuf, 0, sizeof(int)*fdbuflen);
    for (struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
         cmsg != NULL;
         cmsg = CMSG_NXTHDR(&msg, cmsg)) {
        if (cmsg->cmsg_level == SOL_SOCKET && cmsg->cmsg_type == SCM_RIGHTS) {
            int numfds = (cmsg->cmsg_len - sizeof(struct cmsghdr)) / sizeof(int);
            memcpy(fdbuf + 1, CMSG_DATA(cmsg), numfds * sizeof(int));
            fdbuf[0] = numfds;
            break;
        }
    }

    return ret;
}

/* Send data and fds on a Unix domain socket
 *
 * Two buffers can be provided because that's what the message library
 * uses. It may also provide an array of file descriptors to be sent.
 */
int acdbus_send(int fd, char *bv0, size_t size0, char *bv1, size_t size1, int *fdbuf, int numfds)
{
    struct iovec io[2] = {
        { .iov_base = bv0, .iov_len = size0, },
        { .iov_base = bv1, .iov_len = size1, },
    };
    union {
        char buf[CMSG_SPACE(numfds * sizeof(int))];
        struct cmsghdr align;
    } u;
    struct msghdr msg = {
        .msg_iov = io,
        .msg_iovlen = size1 != 0 ? 2 : 1,
    };
    if (numfds != 0) {
        msg.msg_control = u.buf;
        msg.msg_controllen = sizeof u.buf;
        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
        *cmsg = (struct cmsghdr) {
            .cmsg_level = SOL_SOCKET,
            .cmsg_type = SCM_RIGHTS,
            .cmsg_len = CMSG_LEN(numfds * sizeof(int)),
        };
        memcpy(CMSG_DATA(cmsg), fdbuf, numfds * sizeof(int));
    }

    int ret = sendmsg(fd, &msg, 0);
    if (ret < 0) {
        return -errno;
    }

    return ret;
}

/* Close the Unix domain socket with the given file descriptor */
int acdbus_close(int fd)
{
    if (close(fd) < 0) {
        return -errno;
    }

    return 0;
}

uid_t acdbus_geteuid(void)
{
    return geteuid();
}
