;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2019-2020 Göran Weinholt
#!r6rs

;;; Local ("Unix") sockets

;; XXX: Loko extends SRFI 106 to Unix domain sockets, but this library
;; will need to be changed anyway to use recvmsg() and sendmsg() to
;; pass fds

(library (d-bus compat socket)
  (export
    local-connect
    socket-textual-ports

    socket-input-port/fds
    socket-can-pass-fds?
    socket-sendmsg

    socket-close

    close-fdes)
  (import
    (rnrs (6))
    (srfi :106 socket)
    (srfi :106 compat)

    ;; (loko arch amd64 linux-numbers)
    ;; (loko arch amd64 linux-syscalls)
    )

;; Connect to a Unix domain socket (must support abstract sockets!)
(define (local-connect filename)
  (make-client-socket filename "" *af-local* *sock-stream*))

;; Get the file descriptor for a socket (this uses a trick that is
;; likely only going to work with loko-srfi)
(define (%socket-fd sock)
  (let* ((rtd (record-rtd sock))
         (socket-fd (record-accessor rtd 0)))
    (assert (eq? 'fd (vector-ref (record-type-field-names rtd) 0)))
    (socket-fd sock)))

;; Get textual ports for working with a connected socket. Must use
;; eol-style crlf!
(define (socket-textual-ports sock)
  (let ((tc (make-transcoder (utf-8-codec) (eol-style crlf))))
    (let ((i (transcoded-port (socket-input-port sock) tc))
          (o (transcoded-port (socket-output-port sock) tc)))
      (values i o))))

;; Fully sends two bytevectors and a list of file descriptors.
(define (socket-sendmsg sock bv0 size0 bv1 size1 fds)
  (assert (null? fds))
  (socket-send* sock bv0 0 size0)
  (unless (eqv? size1 0)
    (socket-send* sock bv1 0 size1)))

;; Returns a binary input port for the socket and a procedure that can
;; dequeue fds (or #f if not supported) that have been read since the
;; last dequeuing
(define (socket-input-port/fds sock)
  (values (socket-input-port sock)
          #f))

(define (socket-can-pass-fds? sock)
  #f)

;; Close a received file descriptor
(define (close-fdes fd)
  #f))
